import { createRouter, createWebHashHistory } from 'vue-router'
import HeartHome from '@/views/home/HeartHome.vue'
import Index from '@/views/login/Index.vue'
import HeartFriends from '@/views/friends/HeartFriends.vue'
import HeartMessage from '@/views/message/HeartMessage.vue'
import HeartMine from '@/views/mine/HeartMine.vue'
import HeartMusic from '@/views/music/HeartMusic.vue'


import audio1 from '@/views/music/heartmusiclist/audio1.vue'
const routes = [

  {
    path: '',
    redirect: '/HeartHome'
  },
  {
    path: '/HeartHome',
    name: 'HeartHome',
    component: HeartHome
  },
  {
    path: '/Index',
    name: 'Index',
    component: Index
  },

  {
    path: '/HeartFriends',
    name: 'HeartFriends',
    component: HeartFriends
  },
  {
    path: '/HeartMessage',
    name: 'HeartMessage',
    component: HeartMessage
  },
  {
    path: '/HeartMine',
    name: 'HeartMine',
    component: HeartMine
  },
  {
    path: '/HeartMusic',

    name: 'HeartMusic',
    component: HeartMusic,
      //   child:[{
      //       path:'/audio1',
      //       name:'audio1',
      //       component:audio1
      //   }
      // ]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router


